# Programming Things Assignment 1

Alex Law (b5028779)

Pololu Zumo search and rescue operation

### Preface

The words maze, track and map are often used interchangably through this project.  If anything is unclear, please submit an issue.

## Controls

All controls are covered in the GUI provided in the [GUI](/GUI/MainGUI.pde) which makes use of the G4P library.

All controls are a single single character sent over the XBEE.  Every time the Zumo receives an input instruction, an audible beep will be made.  If a beep has not been made the Zumo [may have run out of memory](#memory-limits).

|Key|Operation|Notes|
|:-:|:--------|:----|
|`w` / `a` / `s` / `d`|Movement controls                |`a` and `d` no longer accessibly via GUI|
|`x`        |Stop                                       |Used for debugging purposes
|`q` / `e`  |Nudging the zumo left / right respectively |
|`9`        |Report sensor data over serial             |Used for debugging purposes|
|`8`        |Generate thresholds                        |Used for debugging purposes|
|`0`        |Stop sending sensor information            |Used for debugging purposes|
|`u`        |Turn around                                |Used for debugging purposes|
|`1`        |Autonomously walk to end of corridor       |Task 2|
|`2`        |Search room                                |Task 4|
|`3`        |Indicate that bot has reached a T junction |Task 5|
|`4`        |Return to T junction                       |Task 5 part 2|
|`5`        |Autonomously return back to beginning      |Task 6 - Only works under specific conditions|

## Operation Instructions

1. Turn on the Zumo, wait until an audible beep has been made.
2. Press button on the back of the Zumo, two beeps increasing in pitch will be made produced.
3. Open the GUI.
    1. This can be done by opening [`MainGUI.pde`](/GUI/MainGUI.pde) in [Processing](https://processing.org/download/).
    2. Upon launching the GUI, a `9` character will be sent over the XBEE causing the Zumo to beep once.  If you open the GUI before turning on the Zumo and activating it, the activation tone will change - but everything should still function normally.
4. Place the Zumo within the boundaries of the maze.  Using the GUI, make sure the reflectance sensors are not placed on top of a wall.
5. Press `Start` in the GUI.
6. To handle a corner the Zumo automatically stop and a message will appear in the bottom left text area.  Using the movement controls in the top right turn the corner, and click on `[C]omplete` to resume autonomous navigation.
7. Stop the Zumo to search a room.
    1. Do this by clicking on `Room` as the Zumo is still moving.
    2. Click `Left` or `Right` depending on which side the room is relative to the Zumo.
    3. Using the movement controls, face the entrance of the room.
    4. Click `Left` or `Right` again (doesn't matter which one) to let the Zumo begin scanning the room.
    5. Once complete, navigate the Zumo out of the room.  Make sure it is in the correct orientation, it should be facing the same direction as when you first clicked on `Room`.
    6. Click `Room` to resume autonomous navigation.
8. Indicate a T Junction
    1. Once the Zumo stops at the end of the corridor, you can indicate that this is a T junction by clicking on the `T Junction Here` button.
    2. Turn to face one side of the T junction as if it were any normal corner and click `[C]omplete`.
9. Return to the T junction
    1. Once the Zumo reaches an end of a corridor you can click `Back to Junction`
    2. You will be prompted to turn around, this can be done using the navigation controls, and the `Turn around` button in the top right which is programmed to roughly turn 180 degrees.
    3. Once the Zumo reaches the T junction, it will beep and continue navigating autonomously.
10. Return back to the start, checking rooms along the way
    1. Once the Zumo has reached the end of a corridor, click on `Back to Base`.  The Zumo should then autonomously turn around and navigate to the closest room in which it found a person, followed by the next room until there are no more people to look for, at which point it will return back to the beginning of the maze.  If a person is found the light (PIN 13) will light up.

## Libraries

 - G4P Library (For GUI)
 - NewPing (For Ultrasonic Sensor)
 - Zumo Shield (For the Zumo, DUH!)
 - QList (Linked list implementation)

## Memory limits

Many strings are printed over the serial port across `main.ino`.  Normally these strings are all
stored in RAM, and as a result they take up a significant amount of memory even though they are not
variables.  In order to combat this, the convention is to us the `F()` macro where possible.

At the same time there are many serial outputs that are commented out, feel free to remove these as
they will exist in the git history.  They have been left in for convenient debugging purposes.

Now that the T-junction functionality has been implemented, it has been found that the zumo will run out of memory on runtime and is incapable of complete tasks 1-5 in succession without rebooting before task 5.

## Returning to the T Junction

Upon reaching the T junction, the Zumo should beep and continue to autonomously make its way down the corridor and is interruptible as if it were any other corridor.

## Extras

### Dynamic thresholds

Thresholds to identify the boundaries of the maze are generated at run time, allowing for many materials of surface and walls as long as there is enough contrast between them (the amount of contract is configurable).  It will also allow for some changes mid operation, for example if a lamp is used to illuminate the maze where one half of the map is greatly brighter and gradually fades into darkness on the other side.

### Sensor Data

The Zumo will report back sensor data as it is autonomously travelling, which is displayed in the GUI in the top left.  Useful when initially placing the Zumo down to make sure none of the sensors are accidentally on top of a wall.

### Direction to Rooms

If a person has been found in a room, directions to get to the room are printed in the text area in the bottom right.  An example log file can be found where this data is present - however not in a very human friendly format.

## Strategies

### Pathing

The Zumo uses a stack to store a history of its path in RAM.  Upon some testing it was found that this is not a good strategy at the Zumo has 2kb RAM and will run out in larger mazes.  A solution to this is to store a representation of the maze in the GUI, or another store of memory outside of the Zumo.

#### Maze Representations

In the current implementation a Maze class is used which holds a stack of Move objects.  A better implementation would have been to use a `Corridor` object which are connected to `Room` objects as well as `Corner` objects which can be implemented as a `Corner` or `T Junction`. These classes should be generated by the Zumo as it is travelling and sent over the Serial to be held elsewhere.  And when needed, instructions would be received over the XBEE to navigate the maze according the the task.

Another possible representation is to attempt to store the entire maze as a large grid.  This means that a corridor is comprised of many squares adjacent to each other, and rooms are just a flag on the side of one of these squares.  If we use this, it will mean that we can use the A* pathfinding algorithm to find an efficient route.  A drawback of this is that every turn must be 90 degress where the current implementation does not have this limitation.

### States

The code used to power the Zumo makes use of states.  The behaviour of a method can change depending on the current state, for example searching a room will be different for when it is the Zumo's first time entering the room, and when the Zumo is navigating back to the beginning.

## Credits

Not all code has been written by me.

 - Buzzer sounds are all ripped from the Zumo library example sketches
 - Zumo controls have also been taken and modified from example sketches
 - Ultrasonic sensor init code is taken from NewPing example sketch
 - QList provides a linked list implementation which I use as a stack to store the maze
 - Communications over the serial port are taken and modified from lecture slides