#ifndef MazeH
#define MazeH
#include <QList.h>
#include "Move.h"

class Maze {
public:
    Maze();
    void setPrevTime(unsigned long timestamp);
    void hitWall(unsigned long timestamp);
    void logEndOfCorridor(unsigned long timestamp);
    void logRoom(unsigned long timestamp);
    void logTJunction(unsigned long timestamp);
    void push(Move move);
    bool isEmpty();
    int calculateWallBounceTime();
    Move pushNudges(int duration);
    Move pop();
    QList<Move> getPath();
    int getSize();
private: 
    QList<Move> path;
    unsigned long prevTime; // MS since last move
    int size;
};

#endif