#include "Maze.h"

Maze::Maze() { 
    size = 0;
}

void Maze::logEndOfCorridor(unsigned long timestamp) {
    Move move;
    move.direction = 'f';
    move.duration = timestamp - prevTime;
    Serial.print(F("db:"));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size++;
    path.push_back(move);
    prevTime = timestamp;
}

/**
 * Logs a room that was found.  Only use this when a person is found in the room
 */
void Maze::logRoom(unsigned long timestamp) {
    Move move;
    move.direction = 'R';
    move.duration = 0;
    Serial.print(F("db:"));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size++;
    path.push_back(move);

    prevTime = timestamp;
}

/**
 * Pushes a Move into the stack indicating that the previous item was a t-junction
 */
void Maze::logTJunction(unsigned long timestamp) {
    Move move;
    move.direction = 't';
    move.duration = 0;
    Serial.print(F("db:"));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size++;
    path.push_back(move);
}

void Maze::push(Move move) {
        Serial.print(F("db:"));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size++;
    path.push_back(move);
}

bool Maze::isEmpty() {
    if (path.length() == 0) {
        Serial.print(F("db:stack is empty!"));
    } else {
        Serial.print(F("db:stack is not empty!"));
    }
    return (path.length() == 0);
}

Move Maze::pushNudges(int duration) {
    Move move;
    if (duration > 0) {
        // we turned right
        move.direction = 'r';
        move.duration = duration;
    } else {
        // we turned left
        move.direction = 'l';
        move.duration = -duration;
    }
        Serial.print(F("db:"));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size++;
    path.push_back(move);

    return move;
}

/**
 * Alias for getLastMove()
 */
Move Maze::pop() {
    Move move = path.back();
    Serial.print(F("db:pop "));
    Serial.print(move.direction);
    Serial.print(F(" "));
    Serial.print(move.duration);
    Serial.print(F("!"));
    size--;
    path.pop_back();
    return move;
}

void Maze::setPrevTime(unsigned long timestamp) {
    prevTime = timestamp;
}

void Maze::hitWall(unsigned long timestamp) {
    // This calculation comes from checkIfHittingWall() function
    unsigned long newTime = prevTime + calculateWallBounceTime();
    if (timestamp > newTime) {
        prevTime = newTime;
    } else {
        prevTime = timestamp;
    }
}

int Maze::calculateWallBounceTime() {
    return (50 * 3) + (300 * 2) + 150;
}

QList<Move> Maze::getPath() {
    return path;
}

int Maze::getSize() {
    return size;
}

/**
 * QList need this to be non-member operator
 */
bool operator==(const Move &lhs, const Move &rhs) {
     return (lhs.direction == rhs.direction && lhs.duration == rhs.duration);
}