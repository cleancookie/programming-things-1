import g4p_controls.*;
import processing.serial.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

Serial myPort;   // Create object from Serial class
String val;      // Data received from the serial port
float response;  // Sensor data

void setup() 
{
  myPort = new Serial(this, "COM4" , 9600);
  createGUI();
  customGUI();
  
  myPort.write('9'); // Tell bot to begin sending over debugging info
}

void draw()
{
  try {
    if ( myPort.available() >= 0) {
      val = myPort.readStringUntil('!'); 
      if (isNotEmpty(val)) {
  
        switch (val.substring(0, 3)) {
          case "us:":
            txtUltrasonic.setText(val.substring(3, val.indexOf('!')));
            break;
            
          case "r0:":
            txtReflectance0.setText(val.substring(3, val.indexOf('!')));
            
            response = new Float(val.substring(3, val.indexOf('!')));
            if (response > 500) {
               response = 500;
            }
            slrReflectance0.setValue(response);
            break;
            
          case "r5:":
            txtReflectance5.setText(val.substring(3, val.indexOf('!')));
            
            response = new Float(val.substring(3, val.indexOf('!')));
            if (response > 500) {
               response = 500;
            }
            slrReflectance5.setValue(response);            
            break;
            
          case "t0:":
            txtThreshold0.setText(val.substring(3, val.indexOf('!')));
            
            response = new Float(val.substring(3, val.indexOf('!')));
            if (response > 500) {
               response = 500;
            }
            slrThreshold0.setValue(response);
            break;
            
          case "t5:":
            txtThreshold5.setText(val.substring(3, val.indexOf('!')));
            
            response = new Float(val.substring(3, val.indexOf('!')));
            if (response > 500) {
               response = 500;
            }
            slrThreshold5.setValue(response);
            break;
            
          case "mg:":
            TimeUnit.MILLISECONDS.sleep(50);
            txtMessages.appendText(val.substring(3, val.indexOf('!')) + '\n');
            println(val);
            break;
            
          case "db:":
            TimeUnit.MILLISECONDS.sleep(50);
            // txtDebug.appendText(val.substring(3, val.indexOf('!')) + '\n');
            println(val);
            break;
          
          case "al:":
            txtAlerts.appendText(val.substring(3, val.indexOf('!')) + '\n');
            println(val);
            break;
        } 
      }
    }
  } catch (Exception e) {
    println("An oopsie happened, dont worry about it");
  }
}

public void customGUI() {
}

public boolean isNotEmpty (String str) {
    if(str != null && !str.isEmpty())
        return true;
    return false;
}
