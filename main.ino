#include <Wire.h>
#include <ZumoShield.h>
#include <NewPing.h>
#include "Maze.h"
#include "Move.h"

#define TURN_SPEED 125
// #define TURN_TIME 200
#define SPEED 100
// #define LINE_THICKNESS (double)0.5
// #define INCHES_TO_ZUNITS 17142.0
// #define OVERSHOOT(line_thickness)(((INCHES_TO_ZUNITS * (line_thickness)) / SPEED))
#define PING_MAX 30
#define PING_MIN 4
#define SCAN_AMOUNT 2000
#define END_OF_CORRIDOR_SENS 75
#define NUDGE_AMOUNT 100
#define ADDITIONAL_REFLECTANCE_FOR_THRESHOLD 150
#define TRACK_WIDTH 750
#define REVERSE_AMOUNT 150
#define TIME_TO_180 1000

#define TRIGGER_PIN  2  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     6  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

// This is to stop VSCode's intellisense from complaining
extern HardwareSerial Serial; // Defined somewhere in hardwareSerial.cpp which gets included by Arduino.h

ZumoBuzzer buzzer;
ZumoReflectanceSensorArray reflectanceSensors;
ZumoMotors motors;
Pushbutton button(ZUMO_BUTTON);

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

/**
 * Debugging flag
 * 0 = off
 * 1 = Report ultrasonic sensor distance
 * 2 = Report reflectance
 * 3 = Report ms since start
 * 9 = All
 */
unsigned int debugging = 0;
char inChar; // Last user input
String state = "Init"; // what is the bot currently doing
unsigned int sensors[6]; // reflectance sensor avlues
unsigned int thresholds[6]; // black vs white thresholds, this is genearted
unsigned int pingDistance; // last value of sonar.ping_cm
Maze maze; // keeps a log of all the turns made since starting the maze
const int finishLineSens = 150; // How much of an angle we can hit the finishing line at before we consider it a wall
const int MOVE_PAUSE = 50; // delay between movement to let the bot get back on all fours
const int TURN_REVERSE_AMOUNT = 300; // distance to reverse when turning to stay within walls
const int TURN_AMOUNT = 150; // amount to turn when a wall collision is detected

// Signatures
void turnCorner();
void backToBase();
void tJunctionPart2();
void tJunction();
void searchRoom();
void walkToEndOfCorridor();
void reportReflectance();
void reportUltrasonicSensorDistance();
void processDebug();
void processUserInput();
bool readUserInput();
void reportThresholds();
void executeMove(Move move);
bool checkIfHittingWall(bool *endOfCorridorFound);
void scanLeft(bool *personFound);
void scanRight(bool *personFound);
void crossedFinishingLine();
void manualTurnAround();
void printDirections();

void setup() {
    buzzer.play(">g32");
    Serial.begin(9600);
    digitalWrite(13, HIGH);

    button.waitForButton();
    buzzer.play(">g32>>c32");
    digitalWrite(13, LOW);

    reflectanceSensors.init();
    reflectanceSensors.calibrate();
}

void generateThresholds() {
    reflectanceSensors.read(sensors);
    
    for(int i = 0; i < 6; i++)
    {
        thresholds[i] = sensors[i] + ADDITIONAL_REFLECTANCE_FOR_THRESHOLD;
    }
    Serial.print(F("mg:New thresholds generated!"));
    reportThresholds();

}

void loop() {
    // Get inputs
    readUserInput();
    processUserInput();

    // Debug
    if (millis() % 200 == 0) {
        processDebug();
    }
    Serial.flush();
}

void processDebug() {
    reportUltrasonicSensorDistance();
    reportReflectance();
}

void reportUltrasonicSensorDistance() {
    pingDistance = sonar.ping_cm();
    Serial.print(F("us:")); // Ultrasonic
    Serial.print(pingDistance);
    Serial.print(F("!"));
}

void reportReflectance() {
    reflectanceSensors.read(sensors);
    Serial.print(F("r0:")); // Reflectance sensor
    Serial.print(sensors[0]);
    Serial.print(F("!"));
    Serial.print(F("r5:"));
    Serial.print(sensors[5]);
    Serial.print(F("!"));
}

void reportThresholds() {
    Serial.print(F("t0:")); // Generated threshold values
    Serial.print(thresholds[0]);
    Serial.print(F("!"));
    Serial.print(F("t5:"));
    Serial.print(thresholds[5]);
    Serial.print(F("!"));
}

/**
 *  Gets user input (as a char)
 * 
 * @return bool Return true if inChar has been updated (could be updated to be the same char though!)
 */
bool readUserInput() {
    if (Serial.available() > 0) {
        buzzer.play(">g32");
        inChar = (char)Serial.read();
        return true;
    }

    return false;
}

void processUserInput() {
    switch (inChar) {
    case 'w':
        motors.setSpeeds(SPEED, SPEED);
        break;
    case 'a':
        motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
        break;
    case 's':
        motors.setSpeeds(-SPEED, -SPEED);
        break;
    case 'd':
        motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
        break;
    case 'q':
        motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
        delay(NUDGE_AMOUNT);
        motors.setSpeeds(0, 0);
        inChar = ' ';
        break;
    case 'e':
        motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
        delay(NUDGE_AMOUNT);
        motors.setSpeeds(0, 0);
        inChar = ' ';
        break;
    case 'u':
        motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
        delay(TIME_TO_180);
        motors.setSpeeds(0, 0);
        inChar = ' ';
        break;
    case '0': 
        debugging = 0;
        break;
    case '1': // Task 2
        walkToEndOfCorridor();
        break;
    case '2': // Task 4
        if (state != F("Exiting room") && state != F("Turning around")) {
            searchRoom();
        }
        break;
    case '3': // Task 5
        tJunction();
        break;
    case '4':
        tJunctionPart2();
        break;
    case '5': // Task 6
        backToBase();
        break;
    case '8':
        generateThresholds();
        inChar = ' ';
        break;
    case '9':
        debugging = 9;
        break;
    case 'x':
        motors.setSpeeds(0, 0);
        break;
    default:
        break;
    }
}

boolean aboveLine(int sensorNumber) {
    return sensors[sensorNumber] > thresholds[sensorNumber];
}

void walkToEndOfCorridor() {
    bool endOfCorridorFound = false;

    buzzer.play(">g32");
    state = F("Walking");
    Serial.print(F("mg:Moving to the end of the corridor!"));
    generateThresholds();

    // begin walking forward
    motors.setSpeeds(SPEED, SPEED);
    maze.setPrevTime(millis());
    
    // Autonomous until we find the corner or if the user interrupts
    while(state == F("Walking")) {
        reflectanceSensors.read(sensors);
        if (millis() % 200 == 0) {
            processDebug();
        }

        // Check if the user told us there is a room to search
        if (readUserInput()) {
            processUserInput();
        }

        motors.setSpeeds(SPEED, SPEED);

        // Check if bot hits a wall
        checkIfHittingWall(&endOfCorridorFound);
    }  
    maze.logEndOfCorridor(millis());
    turnCorner(); 
}

bool checkIfHittingWall(bool *endOfCorridorFound) {

    if ((aboveLine(0) && aboveLine(5)) || *endOfCorridorFound) {
        // We hit the end of the corridor
        inChar = ' '; // reset char so it doesnt keep playing the horrific noise when in main loop
        motors.setSpeeds(0, 0);            
        *endOfCorridorFound = true;
        if (state != F("Going back to base")) {
            state = F("Corner found");
        }
    } else if (aboveLine(0) && !aboveLine(5)) {
        // Double check if we actually hit a wall on the left, or did we hit the end but at an angle?
        int attempts = 0;
        while (!*endOfCorridorFound && attempts < 3) {
            attempts++;
            motors.setSpeeds(0,0);
            delay(100);
            Serial.print(F("mg:Looking for left wall attempt: "));
            Serial.print(attempts);
            Serial.print(F("!"));
            processDebug();
            // Only need to check the right most sensor, because the left one might have gone 
            // over the line if the line is very thin
            if (aboveLine(5)) {
                *endOfCorridorFound = true;
                Serial.print(F("mg:Corner found!"));
                state = F("Corner found");
            } else {
                motors.setSpeeds(SPEED / 2, SPEED / 2);
                delay(END_OF_CORRIDOR_SENS);
            }                
        }
        if (!*endOfCorridorFound) {
            // We actually hit a left wall, need to turn right
            // Serial.print("mg:Collision detected on the left!");
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(-SPEED, -SPEED); delay(TURN_REVERSE_AMOUNT);
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(TURN_SPEED, -TURN_SPEED); delay(TURN_AMOUNT);
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(SPEED, SPEED);
            maze.hitWall(millis());
        }
        
    } else if (!aboveLine(0) && aboveLine(5)) {
        // Checking for right wall
        // Double check if we actually hit a wall on the left, or did we hit the end but at an angle?
        int attempts = 0;
        while (!*endOfCorridorFound && attempts < 3) {
            attempts++;
            motors.setSpeeds(0,0);
            delay(100);
            Serial.print(F("mg:Looking for right wall attempt: "));
            Serial.print(attempts);
            Serial.print(F("!"));
            processDebug();
            
            if (aboveLine(0)) {
                *endOfCorridorFound = true;
                Serial.print(F("mg:Wall found!"));
            } else {
                motors.setSpeeds(SPEED / 2, SPEED / 2);
                delay(END_OF_CORRIDOR_SENS);
            }
        }

        if (!*endOfCorridorFound) {
            // We actually hit a left wall, need to turn left
            Serial.print(F("mg:Collision detected on the right!"));
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(-SPEED, -SPEED); delay(TURN_REVERSE_AMOUNT);
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(-TURN_SPEED, TURN_SPEED); delay(TURN_AMOUNT);
            motors.setSpeeds(0, 0); delay(MOVE_PAUSE);
            motors.setSpeeds(SPEED, SPEED);
            maze.hitWall(millis());
        }
    }
}

void scanRight(bool *personFound) {
    unsigned int elapsed = 0;
    unsigned int timeStarted = millis();

    motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
    while (!*personFound && elapsed < SCAN_AMOUNT) {
        processDebug();
        delay(100);
        if (pingDistance < PING_MAX && pingDistance > PING_MIN) {
            *personFound = true;
            Serial.print(F("mg:Person found!"));
            buzzer.play(">g32>>c32");
        } else {
            elapsed = millis() - timeStarted;
        }
    }

    // Return back to normal
    motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
    delay(elapsed);
}

void scanLeft(bool *personFound) {
    unsigned int elapsed = 0;
    unsigned int timeStarted = millis();
    elapsed = 0;
    // Minus 200 from SCAN_AMOUNT because it always over shoots a tiny bit for some reason
    while (!*personFound && elapsed < (SCAN_AMOUNT - 200)) {
        processDebug();
        delay(100);
        if (pingDistance < PING_MAX && pingDistance > PING_MIN) {
            *personFound = true;
            Serial.print(F("mg:Person found!"));
            buzzer.play(">g32>>c32");
        } else {
            elapsed = millis() - timeStarted;
        }
    }

    // Return back to normal
    motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
    delay(elapsed);
}

void printDirections() {
    Serial.print(F("al:DIRECTIONS TO PERSON: !"));
    // For loop couldn't iterate for some reason, using while instead
    int i = 0;
    QList<Move> tempPath = maze.getPath();
    while (i < tempPath.length()) {
        Move tempMove = tempPath.at(i);
        Serial.print("db:");
        Serial.print(i);
        Serial.print('!');
        if (tempMove.direction == 'l') {
            Serial.print(F("al:Turn left!"));
        }

        if (tempMove.direction == 'r') {
            Serial.print(F("al:Turn right!"));
            Serial.print(tempMove.direction);
            Serial.print('!');
        }

        if (tempMove.direction == 'f' && tempMove.duration > 5) {
            Serial.print(F("al:Go forward for "));
            Serial.print(tempMove.duration);
            Serial.print('!');
        }
        i = i + 1;
    }
}

void searchRoom() {
    maze.logEndOfCorridor(millis());

    state = F("Finding room");
    char side = ' '; // l or r
    maze.logEndOfCorridor(millis());

    motors.setSpeeds(0, 0);
    Serial.print(F("mg:Which side is the room on?!"));

    // wait for user input to say which side the room is on
    while (side == ' ') {
        readUserInput();
        if (inChar == 'l' || inChar == 'r') {
            side = inChar;
        }
    }

    // Let the user manually navigate bot into room
    state = "Entering room";
    Serial.print(F("mg:Please turn to face the room on the "));
    if (side == 'l') {
        Serial.print(F("left"));
    } else if (side == 'r') { // if its not l/r for some reason, we will be able to tell
        Serial.print(F("right"));
    }
    Serial.print(F("!"));
    inChar = ' ';
    Serial.print(F("mg:Click left or right once you are done!"));
    int nudgesToEnterRoom = 0;
    while (state == F("Entering room")) {
        readUserInput();
        if (inChar == 'q') {
            nudgesToEnterRoom = nudgesToEnterRoom - NUDGE_AMOUNT;
        } else if (inChar == 'e') {
            nudgesToEnterRoom = nudgesToEnterRoom + NUDGE_AMOUNT;
        }
        processUserInput();

        if (inChar == 'l' || inChar == 'r') {
            state = F("Scanning room");
        }
    }
    // Enter the room
    motors.setSpeeds(SPEED, SPEED);
    delay(TRACK_WIDTH);
    motors.setSpeeds(0, 0);

    // We dont want to log this move yet, just in case we dont find a person
    // maze.pushNudges(nudgesToEnterRoom);

    // Search the room for people
    bool personFound = false;
    scanRight(&personFound);
    scanLeft(&personFound);

    Serial.print(F("mg:Please navigate back into the corridor. Click \"Room\" when done!"));
    state = F("Exiting room");
    motors.setSpeeds(0,0);
    inChar = ' ';

    // let the user exit the room
    while (state == F("Exiting room")) {
        if (readUserInput()) {
            processUserInput();

            if (inChar == '2') {
                Serial.print(F("mg:We are now out of the room!"));

                // Back in the corridor, go back to walking normally
                state = F("Walking");
                break;
            }
        }
    }

    if (personFound) {
        maze.pushNudges(nudgesToEnterRoom);
        maze.logRoom(millis());
        printDirections();
    }

    maze.setPrevTime(millis());
    motors.setSpeeds(0,0);
}

/**
 * Allow the user to manually tun a corner to tell the bot to carry on walking
 */
void turnCorner() {
    buzzer.play(">g32>>g32");
    maze.setPrevTime(millis());

    // Back up a bit since we are on the line already
    motors.setSpeeds(-SPEED, -SPEED);
    delay(REVERSE_AMOUNT);
    motors.setSpeeds(0, 0);

    // Wait for user to turn the corner
    state = F("Turning corner");
    Serial.print(F("mg:Manually turn the corner and then press C to continue!"));
    inChar = ' ';
    Serial.flush();

    int nudges = 0;
    while (state == F("Turning corner")) {
        if (readUserInput()) {

            // Log how long it took us to turn the corner and was it left or right
            // ONLY WORKS IF USER IS USING NUDGES
            if (inChar == 'e') {
                nudges = nudges + NUDGE_AMOUNT;
            } else if (inChar == 'q') {
                nudges = nudges - NUDGE_AMOUNT;
            }
            processUserInput();
        }

        if (inChar == 'c' && state != F("T junction complete")) {
            state = F("Corner turned");
        }

        if (millis() % 200 == 0) {
            processDebug();
        }
    }
    motors.setSpeeds(0, 0);

    // Calculate and push the move onto the maze class 
    if (state = F("Corner turned")) {
        maze.pushNudges(nudges);
    }

    Serial.print(F("mg:Corner turn completed!"));
    walkToEndOfCorridor();
}

void tJunction() {
    Move move;
    motors.setSpeeds(0, 0);
    inChar = ' ';
    char direction = ' ';
    state = F("Turning T junction");
    Serial.print(F("mg:Please turn and press C when you are done!"));
    int nudges = 0;
    while (state == F("Turning T junction")) {
        if (readUserInput()) {
            // Log how long it took us to turn the corner and was it left or right
            // ONLY WORKS IF USER IS USING NUDGES
            if (inChar == 'e') {
                nudges = nudges + NUDGE_AMOUNT;
            } else if (inChar == 'q') {
                nudges = nudges - NUDGE_AMOUNT;
            }
            processUserInput();
        }

        if (inChar == 'c') {
            state = F("T junction turned");
            Serial.print(F("mg:T junction completed!"));
            motors.setSpeeds(0, 0);
        }

        if (millis() % 200 == 0) {
            processDebug();
        }
    }

    maze.pushNudges(nudges);

    // Signify this is a t junction
    maze.logTJunction(millis());

    state = F("T junction complete");
}

void tJunctionPart2() {
    manualTurnAround();

    // Walk back to t junction
    Move move = maze.pop();
    // Keep popping moves till we are at the t junction
    bool isNotEnd = true;
    while (isNotEnd && move.direction != 't') {
        if (move.direction == 't') {
            // We have reached the t junction, just need to turn now
            break;
        }

        if (move.direction == 'R') {
            // This is a room, we want to ignore rooms and go straight to the t junction
            // the next move in the stack will be how we got into the room, we can ignore that
            maze.pop();
            continue;
        }

        if (move.direction == 'l' || move.direction == 'r' || move.direction == 'f') {
            // we only want to turn corners + walk corridors
            executeMove(move);
        }

        // Check if we reached the end
        if (!maze.isEmpty()) {
            move = maze.pop();
        } else {
            state = F("end");
            isNotEnd = false;
            crossedFinishingLine();
        }
    }

    // The next move should be 't'
    // Since we are now visiting the other side, we need to flip the direction we turned so when we
    // are following this path back, it will be facing the way we originally hit the t junction at
    if (move.direction == 't') {
        move = maze.pop();
    }
    if (move.direction == 'l' || 'r' ) {
        int index = maze.getPath().indexOf(move);
        maze.getPath()[index].direction = (move.direction == 'l') ? 'r' : 'l';
        maze.logTJunction(millis());
    }

    inChar = ' ';
    state = F("Waiting to explore other side");
    walkToEndOfCorridor();
}

/**
 * when this sees move.direction == 'l', it will turn RIGHT, this is because we are tracing our path
 */
void executeMove(Move move) {
    // Handle turning
    if ((move.direction == 'l' || move.direction == 'r') && move.duration > 0) {
        if (move.direction == 'l') {
            // he turned LEFT, we are going backwards so we need to turn RIGHT
            motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
        } else if(move.direction == 'r') {
            // he turned RIGHT, we are going backwards so we need to turn LEFT
            motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
        }
        delay(move.duration);

        motors.setSpeeds(0, 0);
    }

    // Handle corridor walking
    if (move.direction == 'f' && move.duration > 0) {
        // Some reason we always overshoot so reduce the duration a bit
        if (move.duration > TRACK_WIDTH) {
            move.duration = move.duration - TRACK_WIDTH;
        }
        // walk forward down corridor while not going over walls
        bool endOfCorridorFound = false;
        unsigned long startTime = millis();
        
        while (!endOfCorridorFound && (millis() - startTime) < move.duration) {
            reflectanceSensors.read(sensors);
            motors.setSpeeds(SPEED, SPEED);
            if (checkIfHittingWall(&endOfCorridorFound)) {
                move.duration += maze.calculateWallBounceTime();
            }
        }
    }

    motors.setSpeeds(0, 0);
}

void manualTurnAround() {
    Serial.print(F("mg:Turn around and press C to continue!"));
    state = F("Turning around");
    while(state == F("Turning around")) {
        if (readUserInput()) {
            processUserInput();

            if (inChar == 'c') {
                state = F("Turned around");
            }
        }
    }
}

void backToBase() {
    state = F("Going back to base");

    motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
    delay(TIME_TO_180);
    motors.setSpeeds(0, 0);

    buzzer.play("L16 cdegreg4");

    while (state == F("Going back to base")) {
        Move move = maze.pop();

        // Prep for t-junction
        if (move.direction == 't') {
            // Next move is how it dealt with the t junction
            move = maze.pop();
        }

        // Handle room
        if (move.duration == 0 && move.direction == 'R') {
            // This is a room, and we only store rooms with people in it so we need to check
            // if the person has evacuated or needs our PIN_13
            Move move = maze.pop();
            executeMove(move);

            // Move into the room
            motors.setSpeeds(SPEED, SPEED);
            delay(TRACK_WIDTH);
            motors.setSpeeds(0, 0);

            // Scan room
            bool personFound = false;
            scanLeft(&personFound);
            scanRight(&personFound);

            if (personFound) {
                // The person is still there, turn on the lights!
                digitalWrite(13, HIGH);
                Serial.print(F("mg:PERSON DID NOT EVACUATE!"));
            } else {
                Serial.print(F("mg:Person evacuated!"));
            }
        }

        executeMove(move);

        if (maze.isEmpty()) {
            crossedFinishingLine();
        }
    }
}

void crossedFinishingLine() {
    Serial.print(F("mg:We have reached the finishing line!"));
    motors.setSpeeds(0, 0);
    buzzer.play("L16 cgcgcg4");
    delay(999999);
}