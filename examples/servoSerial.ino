#include <Servo.h>

Servo middle, left, right, claw ;  // creates 4 "servo objects"
int pins[] = {6, 7, 8, 9};
String names[] = {"middle", "left", "right", "claw"};

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  Serial.println("waiting");
  pinMode(13, OUTPUT);
}

bool running = false;
void loop(){
  int pos = 90;
  if (Serial.available() > 0) {
    char inChar = (char)Serial.read();
    String temp = String(inChar);
    temp.toLowerCase();
    inChar = (char) temp[0] ;
    running = true;
    pos = Serial.parseInt();
    delay(50);
    Serial.print("angle is: ");
    Serial.println(pos);
    Serial.flush();

    switch (inChar) {
      case 'c':
        moveServo(claw, pos, 9);
        break;
      case 'r':
        moveServo(right, pos, 8);
        break;
      case 'l':
        moveServo(left, pos, 7);
        break;
      case 'm':
        moveServo(middle, pos, 6);
        break;
      case  's':
        running = false;
        Serial.println("\t === STOPPED");
   } // end switch
  } // end if

  if (running) {
    Serial.println("running");
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(500);
  }// end if
} // end loop() function______________

void moveServo(Servo s, int angle, int pin) {
  // print message to say which servo is moving...
  // get servo name:
  String servoName = "";
  for (int k = 0; k < 4; k++) {
    if (pins[k] == pin) {
      servoName = names[k];
      break;
    }
  } // end for________
  Serial.print("moving " + servoName + " to position ");
  Serial.println(angle);

  // actually move the servo...
  s.attach(pin);
  s.write(angle);
  delay(500);
  s.detach();
} // end moveServo__________________




