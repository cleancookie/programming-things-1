#ifndef MoveH
#define MoveH

/**
 *  An instruction was that was carried out since the previous move which all combine together to
 * form a list of instructions that the zumo took to get to where it is now.
 * 
 * Rooms and T junctions are shows with a special move with a direction of T or R, followed by
 * another move on how the zumo turned the corner
 */
class Move {
public:
    char direction;
    int duration;
};

#endif